<?php


class CloudImages{

    private $servers = [];
    private $useHttps = true;

    function __construct($servers=[])
    {
        if(is_array($servers)){
            foreach($servers as $key => $value){
                $this->servers[$key] = $value;
            }
        }
    }

    function addServer($value){
        if(is_array($value)){
            $value_array = $value;
            foreach($value_array as $key => $value){
                if(!in_array($value, $this->servers))
                    $this->servers[$key] = $value;
            }
        }else{
            if(!in_array($value, $this->servers))
                $this->servers[] = $value;
        }
    }

    /**
     * @return array
     */
    public function getServers()
    {
        return $this->servers;
    }

    /**
     * @param boolean $useHttps
     */
    public function setUseHttps($useHttps)
    {
        $this->useHttps = $useHttps;
    }

    function createImage($image_link){
        $imageObject = new CloudImagesObject($image_link);
        $imageObject->setServers($this->servers);
        $imageObject->setUseHttps($this->useHttps);

        return $imageObject;
    }

}


class CloudImagesObject{

    private $useHttps = false;
    private $servers = [];
    private $image_link  = null;
    private $size_string = null;
    private static $size_validate = "((s|w|h)[\d]+|w[\d]+-h[\d]+)(|\-[ncpt]+|\-[b]+[0-9\.]+)";

    function __construct($image_link, $size_string=null)
    {
        if(!empty($image_link))
            $this->image_link = $image_link;

        if(!empty($size_string))
            $this->size = $size_string;
    }

    private function newImageObject($link, $size_string){

        //
        $imageObject = new CloudImagesObject($link, $size_string);
        $imageObject->setServers($this->servers);
        $imageObject->setUseHttps($this->useHttps);

        return $imageObject;
    }

    // functions with loop object as result
    function link(){

        // TODO: parse here the vars (size and extensions)
        $url = $this->image_link;

        foreach($this->servers as $from => $to){
            if(!empty($from) && is_string($from)){
                $url = preg_replace('#'.preg_quote($from).'#i', $to, $url);
            }
        }

        if($this->useHttps || preg_match('#^(http[s]?://|//)[^/]+(s3\.|cloudfront\.net)/#i', $url))
            $url = preg_replace('#^http://#i',  "https://", $url); else
            $url = preg_replace('#^https://#i', "http://",  $url);

        //echo $url; exit;

        return $url;
    }

    function size($size_string, $default_no_exists="s480"){

        // link resized
        $link = $this->link();

        // size dont matches
        if(!preg_match("#^".self::$size_validate."$#i", $size_string)
            && preg_match("#^".self::$size_validate."$#i", $default_no_exists)) $size_string = $default_no_exists;

        // link com size já
        if($this->isValidLink()){
            if(preg_match("#^".self::$size_validate."$#i", $size_string, $res_size) && preg_match("#/".self::$size_validate."/([^/]+)$#i", $link, $res)){
                $filename = $res[count($res)-1];
                $link = preg_replace("#/".self::$size_validate."/([^/]+)$#i", '/'.$size_string.'/'.$filename, $link);
            }
            // link não tem size
            if(preg_match("#^".self::$size_validate."$#i", $size_string, $res_size) && !preg_match("#/".self::$size_validate."/([^/]+)$#i", $link)){
                preg_match("#/([^/]+)$#i", $link, $res);
                if($res[1] != ""){
                    $filename = $res[count($res)-1];
                    $link = preg_replace("#/([^/]+)$#i", '/'.$size_string.'/'.$filename, $link);
                }
            }
            //echo $link; exit;
        }

        $imageObject = $this->newImageObject($link, $size_string);

        return $imageObject;
    }

    // forced as jpeg
    function jpg(){
        return $this->changeExtensionTo("jpg");
    }

    function png($alpha=true){

        // TODO: alpha controller
        //if(!$alpha)

        return $this->changeExtensionTo("png");
    }

    // forced as png
    function changeExtensionTo($extension="jpg"){

        $link = preg_replace('#\.([^\.]+)$#i', ".".str_replace('.', '', $extension), $this->image_link);

        $imageObject = $this->newImageObject($link, $this->size_string);
        return $imageObject;
    }

    function isValidLink(){

        // TODO: check valid domains
        $valid = false;
        $url = $this->image_link;

        foreach($this->servers as $from => $to){
            if(!empty($from) && is_string($from)){
                if(preg_match('#'.preg_quote($from).'#i', $url)
                    || preg_match('#'.preg_quote($to).'#i', $url)){
                    $valid = true;
                }
            }
        }

        return $valid;
    }

    /**
     * @return array
     */
    public function getServers()
    {
        return $this->servers;
    }

    /**
     * @param array $servers
     */
    public function setServers($servers)
    {
        $this->servers = $servers;
    }


    function __toString()
    {
        return (string)$this->link();
    }

    /**
     * @param boolean $useHttps
     */
    public function setUseHttps($useHttps)
    {
        $this->useHttps = $useHttps;
    }


}


?>